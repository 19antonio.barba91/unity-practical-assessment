﻿using System.Collections.Generic;
using UnityEngine;

public enum ControllerType 
{
    Null,
    FPSController,
    TPSController
}

public class GameManager: MonoBehaviour {

    public static GameManager instance = null;
    internal bool m_CanSwitch = true;

    [SerializeField] private List<GameObject> m_Controllers = new List<GameObject> ();
    private List<GameObject> m_InstantiateControllers = new List<GameObject> ();
    private int m_IndexController = 0;
    internal ControllerType m_ControllerType = ControllerType.Null;

    private Vector3 m_StartingPosition = new Vector3 (0,1,0);
    private Quaternion m_StartingRotation = new Quaternion (0,0,0,0);
    private Vector3 m_SavedPosition = new Vector3 (0,0,0);
    internal Quaternion m_SavedRotation = new Quaternion (0,0,0,0);

    private Camera m_MainCamera;
    private Vector3 m_SavedMainCameraPosition = Vector3.zero;

    //UI Events
    public delegate void BindingName (string _name);
    public static event BindingName OnChangeName;


    void Awake ()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy (gameObject);

        DontDestroyOnLoad (gameObject);

        InitGame ();
    }

    private void Update ()
    {
        if (Input.GetButtonDown ("Fire1") && m_CanSwitch)
        {
            SwitchContoller ();
        }

        ResetPosition ();
    }

    private void InitGame ()
    {
        for (int i = 0; i < m_Controllers.Count; i++)
        {
            if (m_Controllers[i])
            {
                GameObject controller = Instantiate (m_Controllers[i],m_StartingPosition,m_StartingRotation) as GameObject;
                m_InstantiateControllers.Add (controller);
                controller.name = controller.name.Replace ("(Clone)","");
                controller.SetActive (false);
            }
        }

        m_InstantiateControllers[0].SetActive (true);

        if (OnChangeName != null)
            OnChangeName (m_InstantiateControllers[(m_IndexController + 1) % (m_InstantiateControllers.Count)].name);

        m_MainCamera = Camera.main;
        m_SavedMainCameraPosition = Camera.main.transform.position;
        ToggleCamera ();
    }

    public void SwitchContoller ()
    {
        m_InstantiateControllers[m_IndexController].SetActive (false);
        SaveData (m_InstantiateControllers[m_IndexController].transform.position,m_InstantiateControllers[m_IndexController].transform.rotation);

        m_IndexController = (m_IndexController + 1) % (m_InstantiateControllers.Count);

        LoadData ();
        m_InstantiateControllers[m_IndexController].SetActive (true);


        ToggleCamera ();

        if (OnChangeName != null)
            OnChangeName (m_InstantiateControllers[(m_IndexController + 1) % (m_InstantiateControllers.Count)].name);
    }

    private void SaveData (Vector3 _position, Quaternion _rotation)
    {
        m_SavedPosition = _position;
        m_SavedRotation = _rotation;
    }

    private void LoadData ()
    {
        //Fix position to the ground
        RaycastHit hit;
        if (Physics.Raycast (m_InstantiateControllers[m_IndexController].transform.position, Vector3.down, out hit, 100f))
        {
            Vector3 targetLocation = hit.point;
            targetLocation += new Vector3 (0, transform.localScale.y / 2, 0);
            m_InstantiateControllers[m_IndexController].transform.position = new Vector3(m_SavedPosition.x, targetLocation.y, m_SavedPosition.z);
        }

        m_InstantiateControllers[m_IndexController].transform.rotation = m_SavedRotation;
    }

    private void ResetController ()
    {

    }

    private void ToggleCamera ()
    {
        if (m_MainCamera)
        {
            m_MainCamera.gameObject.SetActive (m_ControllerType == ControllerType.TPSController);
            if (m_MainCamera.gameObject.activeInHierarchy)
                m_MainCamera.transform.position = m_SavedMainCameraPosition;
        } else
            Debug.LogError ("No camera in scene");
    }
    
    private void ResetPosition()
    {
        if(m_InstantiateControllers[m_IndexController].transform.position.y <= -5)
        {
            m_InstantiateControllers[m_IndexController].transform.position = m_StartingPosition;
            m_InstantiateControllers[m_IndexController].transform.rotation = m_StartingRotation;

            SaveData (m_StartingPosition ,m_StartingRotation);
        }
    }
}
