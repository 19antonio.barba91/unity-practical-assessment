﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ButtonText : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

    private Text m_Text;
    
    private void Awake()
    {
        m_Text = GetComponent<Text> ();
    }

    void OnEnable ()
    {
        GameManager.OnChangeName += OnBindingName;
    }


    void OnDisable ()
    {
        GameManager.OnChangeName -= OnBindingName;
    }

    private void OnBindingName(string _name)
    {
        m_Text.text = _name;
    }

    public void OnPointerEnter (PointerEventData eventData)
    {
        GameManager.instance.m_CanSwitch = false;
    }

    public void OnPointerExit (PointerEventData eventData)
    {
        GameManager.instance.m_CanSwitch = true;
    }

}
